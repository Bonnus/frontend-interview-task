import React, {useState} from "react";
import {useHistory} from "react-router";
import {LOGIN_STEP_1} from "../../constants/routerConstants.ts";
import {SESSION_EMAIL_KEY} from "../../constants/sessionConstants.ts";
import Modal from "../../components/Modal";

const SUCCESS_TEXT = 'Success!'

function Step2() {
    const email = sessionStorage.getItem(SESSION_EMAIL_KEY) ?? ''
    const history = useHistory();
    const [isShowModal, setShowModal] = useState(false)

    const [textForModal, setTextForModal] = useState('')

    const handleBack = () => {
        history.push(LOGIN_STEP_1)
    }

    const handleSendEmail = async () => {
        const body = JSON.stringify({email})
        try {
            const res = await fetch('http://localhost:4040/endpoint', {
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST', body
            })
            const resultData = await res.json();
            if (resultData.ok) {
                setShowModal(true)
                setTextForModal(SUCCESS_TEXT)
            } else {
                setTextForModal(resultData.message)
            }
        } catch (error) {
            console.error('error', error)
        }
    }

    const handleClose = () => {
        setShowModal(false)
    }

    return (
        <>
            <div className="form-control">
                <label className="label">
                    <span className="label-text">Email</span>
                </label>
                <input onChange={() => null} value={email} type="text" placeholder="Type here" className="input"/>
            </div>

            <div className="mt-auto">
                <button
                    onClick={handleBack}
                    className="btn btn-ghost w-1/2">
                    Back
                </button>
                <button
                    onClick={handleSendEmail}
                    className="btn btn-primary w-1/2">
                    Confirm
                </button>
            </div>
            {isShowModal && <Modal handleClose={handleClose} text={textForModal}/>}
        </>
    )
}

export default Step2
