import React from "react";
import Step1 from "./Step1";
import {Redirect, Route, Switch} from "react-router-dom";
import {LOGIN_STEP_1, LOGIN_STEP_2} from "../constants/routerConstants.ts";
import Step2 from "./Step2";
import {SESSION_EMAIL_KEY} from "../constants/sessionConstants.ts";

function LoginPage() {
    const email = sessionStorage.getItem(SESSION_EMAIL_KEY) ?? ''

    return (
        <Switch>
            <Route path={LOGIN_STEP_1} component={Step1}/>
            {!!email && <Route path={LOGIN_STEP_2} component={Step2}/>}
            <Redirect to={LOGIN_STEP_1}/>
        </Switch>
    )
}

export default LoginPage
