import React, {ChangeEvent, useState} from "react";
import {useHistory} from "react-router";
import {LOGIN_STEP_2} from "../../constants/routerConstants.ts";
import {SESSION_EMAIL_KEY} from "../../constants/sessionConstants.ts";

const HOLD_TIME = 500

const isValidEmail = (email: string) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
}

function Step1() {
    const history = useHistory();

    const [email, setEmail] = useState(sessionStorage.getItem(SESSION_EMAIL_KEY) ?? '');
    const [checked, setChecked] = useState(false);
    const [, setTimeoutId] = useState<NodeJS.Timeout>();
    const [, setIntervalId] = useState<NodeJS.Timeout>();
    const [elapsedTime, setElapsedTime] = useState<number>(HOLD_TIME);

    const handleChangeEmail = (event: ChangeEvent<HTMLInputElement>) => {
        const newEmail = event.target.value
        setEmail(newEmail)
        if (isValidEmail(newEmail)) {
            sessionStorage[SESSION_EMAIL_KEY] = newEmail
        }
    }

    const handleChangeCheckbox = (event: ChangeEvent<HTMLInputElement>) => {
        setChecked(event.target.checked)
    }

    const handleMouseDown = () => {
        const newIntervalId = setInterval(() => {
            setElapsedTime((currentElapsedTime) => {
                return currentElapsedTime - 10
            })
        }, 10);

        setIntervalId(newIntervalId)

        const newTimeoutId = setTimeout(() => {
            history.push(LOGIN_STEP_2);
            clearInterval(newIntervalId);
        }, HOLD_TIME);

        setTimeoutId(newTimeoutId)
    }

    const handleMouseUp = () => {
        setIntervalId((currentIntervalId) =>{
            clearInterval(currentIntervalId);
            return undefined
        })
        setTimeoutId((currentTimeoutId) =>{
            clearTimeout(currentTimeoutId);
            return undefined
        })
        setTimeoutId(undefined)
        setElapsedTime(HOLD_TIME)
    }

        return (
        <>
            <div className="form-control">
                <label className="label">
                    <span className="label-text">Email</span>
                </label>
                <input onChange={handleChangeEmail} value={email} type="text" placeholder="Type here"
                       className="input"/>
            </div>
            <div className="p-1"></div>
            <div className="form-control">
                <label className="label cursor-pointer justify-start gap-2">
                    <input onChange={handleChangeCheckbox} checked={checked} type="checkbox"
                           className="checkbox checkbox-primary"/>
                    <span className="label-text">I agree</span>
                </label>
            </div>
            <button
                onMouseDown={handleMouseDown}
                onMouseUp={handleMouseUp}
                disabled={!checked || !isValidEmail(email)}
                className="btn btn-primary mt-auto">
                Hold to proceed {elapsedTime}ms
            </button>
        </>
    )
}

export default Step1
