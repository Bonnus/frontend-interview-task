import {BrowserRouter} from 'react-router-dom'
import React from "react";
import Router from "./router.tsx";
import Layout from "./containers/components/Layout";

export default function App() {
    return (
        <BrowserRouter>
            <Layout>
                <Router />
            </Layout>
        </BrowserRouter>
    )
}
