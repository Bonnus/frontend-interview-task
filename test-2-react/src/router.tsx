import * as React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import LoginPage from "./containers/LoginPage";
import {LOGIN_STEP_1} from "./containers/constants/routerConstants.ts";

const Router = () => {
    return (
        <Switch>
            <Route path="/login/:step" component={LoginPage}/>
            <Redirect to={LOGIN_STEP_1}/>
        </Switch>
    );
};

export default Router;
